import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from './users/users.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      'mongodb+srv://nguyenchitrung1310:admin123@clusternestjs.zwrugjn.mongodb.net/',
    ),
    UserModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
