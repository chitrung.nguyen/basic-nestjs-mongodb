import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';

@Schema({
  collection: 'users',
  timestamps: true,
  id: false,
  versionKey: false,
  toObject: { virtuals: true },
  toJSON: {
    virtuals: true,
    getters: true,
    transform: function (doc, ret) {
      delete ret.password;
    },
  },
})
export class User {
  @Prop({ unique: true, required: true })
  username: string;

  @Prop({ unique: true, required: true })
  password: string;

  @Prop({ required: false })
  displayName?: string;

  @Prop({ required: false })
  avatarUrl?: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
